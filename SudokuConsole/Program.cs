﻿using SudokuLib;
using SudokuLib.Solvers;
using System;

namespace SudokuConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var sudokuStr = "3.7..4.2....1..8..9............3..9..5.8......4.6...........5.12...7..........6..";

            if (args.Length == 1)
            {
                sudokuStr = args[0];
            }

            //var sudokuStr = "1234567894567891237891.345623456789156789123489123456734.678912678912345912345678";

            var sudoku = new SudokuGrid(sudokuStr);
            var solver = new SudokuColoringGraphSolver();
            var solvedSudoku = solver.Solve(sudoku);

            Console.WriteLine(solvedSudoku);
            Console.ReadKey();
        }
    }
}
