﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SudokuLib.Convertes;
using System;

namespace SudokuLibTest
{
    [TestClass]
    public class SudokuGridCoordinateConverterTest
    {
        private SudokuGridCoordinateConverter Converter { get; } = new SudokuGridCoordinateConverter();


        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Convert_WhenXOverEight_ShouldThrowArgumentOutOfRangeException()
        {
            Converter.Convert(9, 5);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Convert_WhenXUnderZero_ShouldThrowArgumentOutOfRangeException()
        {
            Converter.Convert(-1, 5);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Convert_WhenYOverEight_ShouldThrowArgumentOutOfRangeException()
        {
            Converter.Convert(5, 9);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Convert_WhenYUnderZero_ShouldThrowArgumentOutOfRangeException()
        {
            Converter.Convert(5, -1);
        }
    }
}
