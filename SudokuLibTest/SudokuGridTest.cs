﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SudokuLib;
using System;
using System.Linq;

namespace SudokuLibTest
{
    [TestClass]
    public class SudokuGridTest
    {
        private string validString = "1234567894567891237891.345623456789156789123489123456734.678912678912345912345678";
        private SudokuGrid grid;

        [TestInitialize]
        public void TestInit()
        {
            grid = new SudokuGrid(validString);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Ctor_WhenStringContainsAnInvalidToken_ShouldThrowArgumentException()
        {
            var invalidToken = "000000068900000002000400500041000000000035000050000000000800010300000700000100400";
            var grid = new SudokuGrid(invalidToken);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Ctor_WhenStringSizeIsInvalid_ShouldThrowArgumentException()
        {
            var invalidSize = "3456789.456789";
            var grid = new SudokuGrid(invalidSize);
        }

        [TestMethod]
        public void Ctor_WhenStringIsValid()
        {
            var grid = new SudokuGrid(validString);
            var values = string.Join("", validString);

            Assert.IsTrue(Enumerable.SequenceEqual(grid.Grid, values));
        }

        [TestMethod]
        public void ElementAt_WhenParametersAreValid_ShouldReturnTheCorrectElement()
        {
            Assert.AreEqual(grid.ElementAt(0, 0), validString[0]);
            Assert.AreEqual(grid.ElementAt(1, 0), validString[1]);
            Assert.AreEqual(grid.ElementAt(1, 5), validString[grid.NCols * 5 + 1]);
        }

        [TestMethod]
        public void SetElementAt_WhenParametersAreValid_ShouldSetTheValue()
        {
            var newValue = '5';
            var x = 5;
            var y = 8;

            grid.SetElementAt(x, y, newValue);
            Assert.AreEqual(grid.ElementAt(x, y), newValue);
        }
    }
}
