﻿namespace GraphLib.Interfaces
{
    interface IGraph<NodeT>
    {
        void AddNode(NodeT node);
        void AddEdge(NodeT from, NodeT to);
        bool AreLinked(NodeT a, NodeT b);
    }
}
