﻿using GraphLib.Nodes;
using System.Collections.Generic;
using System.Linq;

namespace GraphLib.Algorithms
{
    public class BruteForceGraphColoring
    {
        int[] Colors { get; }
        AdjacencyList Graph { get; }

        public BruteForceGraphColoring(IEnumerable<int> colors, AdjacencyList graph)
        {
            Colors = colors.ToArray();
            Graph = graph;
        }

        public void Compute()
        {
            if (!Compute(0))
            {
                throw new System.Exception("The graph cannot be colored");
            }
        }

        public bool Compute(int idx)
        {
            if (idx == Graph.Nodes.Length)
            {
                return true;
            }
            else if (Graph.Nodes[idx].IsColorFixed)
            {
                return Compute(idx + 1);
            }

            var neighborsColor = Graph.Neighbors[idx].Select(n => Graph.Nodes[n].Color).Distinct();
            foreach (var c in Colors)
            {
                if (!neighborsColor.Contains(c))
                {
                    Graph.Nodes[idx].Color = c;

                    if (Compute(idx + 1))
                    {
                        return true;
                    }
                }
                Graph.Nodes[idx].Color = ColoredNode.BlankColor;
            }

            return false;
        }
    }
}
