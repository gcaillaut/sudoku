﻿using GraphLib.Nodes;
using System.Collections.Generic;
using System.Linq;

namespace GraphLib.Algorithms
{
    public class GreedyGraphColoring
    {
        IEnumerable<int> Colors { get; }
        AdjacencyList Graph { get; }

        public GreedyGraphColoring(IEnumerable<int> colors, AdjacencyList graph)
        {
            Colors = colors;
            Graph = graph;
        }

        public void Compute()
        {
            foreach (var c in Colors)
            {
                for (var i = 0; i < Graph.Nodes.Length; ++i)
                {
                    if (Graph.Nodes[i].Color == ColoredNode.BlankColor)
                    {
                        var neighborsColor = Graph.Neighbors[i].Select(n => Graph.Nodes[n].Color).Distinct();
                        if (!neighborsColor.Contains(c))
                        {
                            Graph.Nodes[i].Color = c;
                        }
                    }
                }
            }

            if (Graph.Nodes.Any(n => n.Color == ColoredNode.BlankColor))
            {
                throw new System.Exception("The graph cannot be colored");
            }
        }
    }
}
