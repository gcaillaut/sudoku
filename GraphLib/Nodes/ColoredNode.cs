﻿namespace GraphLib.Nodes
{
    public class ColoredNode
    {
        public int Id { get; }
        public int Color { get; set; }
        public bool IsColorFixed { get; }

        public static readonly int BlankColor = -1;

        public ColoredNode(int id)
        {
            Id = id;
            Color = -1;
            IsColorFixed = false;
        }

        public ColoredNode(int id, int color, bool isColorFixed)
        {
            Id = id;
            Color = color;
            IsColorFixed = isColorFixed;
        }
    }
}
