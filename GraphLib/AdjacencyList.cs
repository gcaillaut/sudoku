﻿using GraphLib.Nodes;
using System.Collections.Generic;
using System.Linq;

namespace GraphLib
{
    public class AdjacencyList
    {
        public ColoredNode[] Nodes { get; }
        public ICollection<int>[] Neighbors { get; }
        public bool IsDirected { get; }

        public AdjacencyList(int nbNodes, bool isDirected = false)
        {
            Nodes = Enumerable.Range(0, nbNodes).Select(i => new ColoredNode(i)).ToArray();
            Neighbors = Enumerable.Range(0, nbNodes).Select(i => new List<int>()).ToArray();
            IsDirected = isDirected;
        }

        public AdjacencyList(IEnumerable<ColoredNode> nodes, bool isDirected = false)
        {
            Nodes = nodes.ToArray(); ;
            Neighbors = Enumerable.Range(0, Nodes.Length).Select(i => new List<int>()).ToArray();
            IsDirected = isDirected;
        }

        public void AddEdge(int from, int to)
        {
            Neighbors[from].Add(to);
            if (!IsDirected)
            {
                Neighbors[to].Add(from);
            }
        }

        public bool AreLinked(int a, int b)
        {
            return Neighbors[a].Contains(b);
        }
    }
}
