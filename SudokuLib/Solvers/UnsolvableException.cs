﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuLib.Solvers
{
    class UnsolvableException : Exception
    {
        public UnsolvableException() : base("Object cannot be solved")
        {

        }
    }
}
