﻿using GraphLib;
using GraphLib.Algorithms;
using GraphLib.Nodes;
using SudokuLib.Convertes;
using SudokuLib.Interfaces;
using System.Linq;

namespace SudokuLib.Solvers
{
    public class SudokuColoringGraphSolver : ISolver<SudokuGrid>
    {
        public SudokuGrid Solve(SudokuGrid objectToSolve)
        {
            var graph = new AdjacencyList(objectToSolve.Grid.Select((n, i) => new ColoredNode(i, n - '0', n != SudokuGrid.NullValue)), false);
            var converter = new SudokuGridCoordinateConverter();

            for (int i = 0; i < 81; i++)
            {
                int x, y;
                converter.ConvertBack(i, out x, out y);
                var idx = i + 1;

                while (idx % 9 != 0)
                {
                    graph.AddEdge(i, idx);
                    ++idx;
                }

                idx = i + 9;
                while (idx < 81)
                {
                    graph.AddEdge(i, idx);
                    idx += 9;
                }
            }


            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    var from = converter.Convert(i * 3, j * 3);
                    graph.AddEdge(from, from + 1 + 18);
                    graph.AddEdge(from, from + 2 + 9);
                    graph.AddEdge(from, from + 2 + 18);

                    from += 1;
                    graph.AddEdge(from, from - 1 + 9);
                    graph.AddEdge(from, from + 1 + 9);
                    graph.AddEdge(from, from - 1 + 18);
                    graph.AddEdge(from, from + 1 + 18);

                    from += 1;
                    graph.AddEdge(from, from - 1 + 18);
                    graph.AddEdge(from, from - 2 + 9);
                    graph.AddEdge(from, from - 2 + 18);

                    from += 9 - 2;
                    graph.AddEdge(from, from + 1 + 9);
                    graph.AddEdge(from, from + 2 + 9);

                    from += 1;
                    graph.AddEdge(from, from - 1 - 9);
                    graph.AddEdge(from, from + 1 - 9);
                    graph.AddEdge(from, from - 1 + 9);
                    graph.AddEdge(from, from + 1 + 9);

                    from += 1;
                    graph.AddEdge(from, from - 1 + 9);
                    graph.AddEdge(from, from - 2 + 9);
                }
            }

            var colors = Enumerable.Range(1, 9);
            var solver = new BruteForceGraphColoring(colors, graph);
            solver.Compute();

            var ary = graph.Nodes.Select(n => n.Color).ToArray();
            var SudokuStr = string.Join("", ary);
            var solvedSudokuGrid = new SudokuGrid(SudokuStr);
            return solvedSudokuGrid;
        }

        public bool TrySolve(SudokuGrid objectToSolve, out SudokuGrid solvedObject)
        {
            var isSolved = true;
            try
            {
                solvedObject = Solve(objectToSolve);
            }
            catch (UnsolvableException)
            {
                solvedObject = null;
                isSolved = false;
            }

            return isSolved;
        }
    }
}
