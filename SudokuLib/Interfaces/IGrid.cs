﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SudokuLib.Interfaces
{
    public interface IGrid<T>
    {
        T[] Grid { get; }
        int NCols { get; }
        int NRows { get; }

        T ElementAt(int x, int y);
        void SetElementAt(int x, int y, T value);
    }
}
