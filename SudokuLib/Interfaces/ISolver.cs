﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuLib.Interfaces
{
    public interface ISolver<T>
    {
        T Solve(T objectToSolve);
        bool TrySolve(T objectToSolve, out T solvedObject);
    }
}
