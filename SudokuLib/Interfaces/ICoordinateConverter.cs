﻿namespace SudokuLib.Interfaces
{
    public interface ICoordinateConverter
    {
        int Convert(int x, int y);
        void ConvertBack(int idx, out int x, out int y);
    }
}
