﻿using SudokuLib.Interfaces;
using System;

namespace SudokuLib.Convertes
{
    public class SudokuGridCoordinateConverter : ICoordinateConverter
    {
        public int NCols { get; } = 9;
        public int NRows { get; } = 9;

        public int Convert(int x, int y)
        {
            if (x >= NCols || x < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(x), $"NCols is in range [0..{NCols - 1}], found {x}");
            }
            if (y >= NRows || y < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(x), $"NRows is in range [0..{NRows - 1}], found {y}");
            }

            return y * NCols + x;
        }

        public void ConvertBack(int idx, out int x, out int y)
        {
            if (idx >= NCols * NRows || idx < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(idx), $"Index must be in range [0..{NCols * NRows - 1}], found {idx}");
            }

            y = idx / NCols;
            x = idx - y;
        }
    }
}
