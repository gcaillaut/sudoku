﻿using SudokuLib.Convertes;
using SudokuLib.Interfaces;
using System;
using System.Linq;
using System.Text;

namespace SudokuLib
{
    public class SudokuGrid : IGrid<char>
    {
        public char[] Grid { get; }
        public int NCols { get; } = 9;
        public int NRows { get; } = 9;
        public ICoordinateConverter CoordinateConverter { get; } = new SudokuGridCoordinateConverter();

        public static char NullValue { get; } = '.';
        public static string ValidTokens = ".123456789";

        public SudokuGrid(string gridStr)
        {
            if (gridStr.Length != NCols * NRows)
            {
                throw new ArgumentException($"A sudoku grid must be 9x9 (81) sized, found {gridStr.Length}", nameof(gridStr));
            }

            Grid = gridStr.Select(c =>
            {
                if (!ValidTokens.Contains(c))
                {
                    throw new ArgumentException($"Tokens must be one of '{ValidTokens}', but is '{c}'", nameof(gridStr));
                }

                return c;
            }).ToArray();
        }

        public char ElementAt(int x, int y)
        {
            return Grid[CoordinateConverter.Convert(x, y)];
        }

        public void SetElementAt(int x, int y, char value)
        {
            Grid[CoordinateConverter.Convert(x, y)] = value;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            var sep = "+---+---+---+";
            sb.AppendLine(sep);

            for (int i = 0; i < 9; i++)
            {
                sb.Append('|');
                sb.Append(string.Join("", Grid.Skip(i * 9).Take(3).Select(n => n)));
                sb.Append('|');
                sb.Append(string.Join("", Grid.Skip(i * 9 + 3).Take(3).Select(n => n)));
                sb.Append('|');
                sb.Append(string.Join("", Grid.Skip(i * 9 + 6).Take(3).Select(n => n)));
                sb.AppendLine("|");
                if (i % 3 == 2)
                {
                    sb.AppendLine(sep);
                }
            }

            return sb.ToString();
        }
    }
}
