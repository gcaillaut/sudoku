## Défi de Clem : solveur de Sudoku

Un solveur de Sudoku développé en C#.
Le solveur se base sur un algortihme de coloration de graphe.

Un graphe est construit à partir d'une grille de Sudoku : un noeud sera représenté par une case de la grille.
Chaque noeud a 20 voisins : les 8 cases de la ligne, les 8 de la colonne et les 4 du carré qui ne ne sont ni sur la même ligne, ni sur la même colonne.
Un algorithme de coloration est ensuite appliqué à ce graphe. Il y a 9 couleurs possibles puisque les case ne peuvent prendre que les valeurs de 
l'intervalle [1, 9].
Si le graphe est 9 coloriable, alors la coloration est converti en une grille de Sudoku : c'est la grille résolue.